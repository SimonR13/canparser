import os, re, sys, binascii
from os import walk
from datetime import datetime

newFiles = list()
setOfIds = set()
idMsgDict = dict()

def hexToInt(hexString):
    retVal = ""
    for idx in range(0, len(hexString), 2):
        tmpString = hexString[idx] + hexString[idx+1]
        retVal = retVal + " " + str(int(tmpString, 16))
    return(retVal)

def saveToNewFile(rootpath):
    global setOfIds, idMsgDict
    dateStr = datetime.now().strftime("%Y%m%d")
    timeStr = datetime.now().strftime("%H%M%S")
    with open(rootpath+dateStr+"_parsedOutput_"+timeStr+".log", "wr") as outFile:
        outFile.write("Start of File with parsed CAN Messages\n")
        for msgId in sorted(setOfIds):
            outFile.write("Msgs for ID {0:X}: \n".format(msgId))
            for key, value in sorted(idMsgDict[msgId].iteritems()):                
                outFile.write("\t {0:s}\t{1:s}\t{2:s}\n".format(key, binascii.unhexlify(value), hexToInt(value)))
            outFile.flush()
        outFile.write("End of File with parsed CAN messages")

def parseFile(path):
    global setOfIds, idMsgDict
    retval = 1
    with open(path) as inFile:
        print("parsing file {0}\n".format(path))
        lines = inFile.readlines()
        for idx in range(len(lines)):
            line = lines[idx]
            if (line.count(str('#')) == 1):
                idMsgSplit = line.split('#')
                try:
                    idInt = int(idMsgSplit[0], 16)
                    if idInt not in setOfIds:
                        setOfIds.add(idInt)
                        idMsgDict[idInt] = dict()
                    msgHex = idMsgSplit[1].rstrip().lstrip()
                    idMsgDict[idInt][msgHex] = msgHex
                except:
                    print("something went wrong:\n Exception: {0}\n Line: {1:s} ".format( sys.exc_info()[0], line))
                
            if (line.count(str('#')) > 1):
                print("special file: {0}\n".format(path))
                retval = 0
    return retval
                
                                

def handleNewFiles(rootpath = "/home/simon/Documents/car/canLogs/"):
    for (dirpath, dirnames, filenames) in walk(rootpath):
        for filename in filenames:
            if filename.find("parsedOutput") != -1:
                continue
            if (parseFile(rootpath + filename)):
                continue
                os.rename(rootpath + filename, rootpath + "parsed/" + filename)
            else:
                os.rename(rootpath+filename, rootpath+"special/"+filename)
        break
    saveToNewFile(rootpath)
    
    
        
