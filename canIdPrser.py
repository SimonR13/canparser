import os, re

completeIdMap = dict()
idMsgsDict = dict()

def check(fname, txt):
    with open(fname) as dataf:
        return any(txt in line for line in dataf)

def populateSet(file):
    regex = r"\d+"
    tmpSet = set()
    for line in file:
        id = re.search(regex, line)
        if id is not None:
            tmpSet.add(int(id.group(0)))

def parseFileForIdMsgs(path, mode):
    global idMsgsDict
    with open(path, mode) as f:
        lines = f.readlines()
        for idx in range(len(lines)):
            line = lines[idx]
            if (line.find(str('Msg')) != -1):
                listOffset = 1
                try:
                    idStr = lines[idx + listOffset]
                    id = int(idStr)
                    if id not in idMsgsDict.iterkeys():
                        idMsgsDict[id] = list()
                    while (idx + listOffset + 1 < len(lines)):
                        listOffset += 1
                        dataLine = lines[idx + listOffset]
                        if (dataLine.find(str('Msg')) == -1):
                            # next line is data
                            idMsgsDict[id].append(dataLine)
                        else:
                            break
                except ValueError:
                    continue
                        
                        

def parseFile( path, mode):
    global completeIdMap
    idAnzahlMap = dict()
    f = open(path, mode)
    while True:
        line = f.readline()
        if len(line) == 0:
            break
        if line.find(str('Msg')) != -1:
            try:
                id = int(f.readline())
                if id not in idAnzahlMap.iterkeys():
                    idAnzahlMap[id] = 1
                else:
                    idAnzahlMap[id] += 1
            except ValueError:
                continue
    completeIdMap[f.name] = idAnzahlMap
    f.close()

def runStd4Msgs():
    global idMsgsDict
    parseFileForIdMsgs('/home/simon/CAN_LOG_WITH_RNS.log', 'r')
    parseFileForIdMsgs('/home/simon/20161124_Arduino_170735.log', 'r')

    with open('/home/simon/20161210_IDs_Msgs.log', 'w') as fOut:
        for key in sorted(idMsgsDict.iterkeys()):
            fOut.write('Messages for ID {0:x}:\n'.format(key))
            for data in idMsgsDict[key]:
                for byteStr in data.split():
                    output = parseByte(byteStr)
                    fOut.write('{0:x} '.format(output))
                fOut.write('\n')
            fOut.write('\n')
            fOut.flush()
        fOut.write('Ende\n')  

def runStandard():
    parseFile('/home/simon/CAN_LOG_WITH_RNS.log', 'r')
    parseFile('/home/simon/20161124_Arduino_170735.log', 'r')

    fOut = open('/home/simon/20161130_canIDs', 'w')
    distinctIdMap = dict()
    for key, idAnzahlMap in completeIdMap.iteritems():
        fOut.write('----{0}----\n'.format(key))
        for idNr in sorted(idAnzahlMap.iterkeys()):
            if idNr not in distinctIdMap:
                distinctIdMap[idNr] = idAnzahlMap[idNr]
            fOut.write('id[ dec: {0:d} - hex: {0:x} - bin: {0:b}]\n'.format(idNr))
        fOut.write('----ende----\n')

    fOut.write('distinct start\n')
    for distinctNr, anzahl in sorted(distinctIdMap.iteritems(), key=lambda (k,v): (v,k)):
        fOut.write('id[ dec: {0:d} - hex: {0:x} - bin: {0:b}]:{1:d}\n'.format(distinctNr, anzahl))
    fOut.write('distinct ende\n')
    fOut.flush()
    fOut.close()
    
    
def run():
    #if not os.path.exists('/home/simon/20161124_canIDs'):
    fOut = open('/home/simon/20161130_canIDs', 'w')
    fOut.write('sof\n')
    fOut.flush()
    fOut.close()
        
    while True:
        userInput = raw_input('Bitte Pfad angeben:\n')
        if len(userInput) < 1:
            break
        parseFile(userInput, 'r')

    fOut = open('/home/simon/20161130_canIDs', 'a')
    for key, idSet in completeIdMap.iteritems():
        for idNr in idSet:
            fOut.write('file{1}-id[ dec: {0:d} - hex: {0:x} - bin: {0:b}]\n'.format(idNr, key))
    fOut.write('eof\n')    
    fOut.close()


def parseByte( byteStr ):
    try:
        retVal = int(byteStr)
    except ValueError:
        return 0
    return retVal


            
                               
